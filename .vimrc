execute pathogen#infect()

"let g:session_autoload = 'yes'
let g:session_autosave = 'yes'
let g:session_autosave_silent = 1
let g:session_default_overwrite = 1
let g:session_autosave_periodic = 20
let g:session_directory = getcwd()
let g:cpp_class_scope_highlight = 1
let g:cpp_experimental_template_highlight = 1

" #################################    basic stuff    #######################################
syntax on           		" syntax coloring
colorscheme desert  		" sets default color scheme
set nu              		" show line numbers
set textwidth=0 wrapmargin=0
" auto reload files after 4 sec
set autoread
au CursorHold * checktime

set expandtab         		" replace tabs with spaces
set tabstop=4         		" how many columns a tab counts for
set shiftwidth=4			" control how many columns text is indented with the reindent operations

" highlight column
set colorcolumn=120
highlight ColorColumn ctermbg=darkgray

set history=50        		" keep 50 lines of command line history
set hlsearch          		" highlight searched text
set incsearch         		" do incremental searching
set laststatus=2      		" always show status line
set mouse=a           		" don't copy line numbers when marking in noGUI
set ruler             		" show the cursor position all the time
set showcmd           		" display incomplete commands
set showmatch         		" cursor will briefly jump to the matching brace when you insert one

set smartindent		  		" automatically inserts one extra level of indentation in some cases, and works for C-like files
set so=5              		" scrolls the text so that there are always at least five lines visible above the cursor

set guifont=Monospace\ 8	" set GUI font

" highlight ExtraWhitespace at end of line, remove them at save buffer ######################
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

function! TrimWhiteSpace()
        %s/\s\+$//e
endfunction
autocmd BufWritePre     *.cpp :call TrimWhiteSpace()
autocmd BufWritePre     *.hpp :call TrimWhiteSpace()
autocmd BufWritePre     *.ttcn3 :call TrimWhiteSpace()
" ###########################################################################################

filetype plugin indent on	" automatically finds and load specific plugin or indent file for known files

" function for switching between cpp and hpp files ##########################################
"function! Switch_HPP_CPP()
    "if match(expand("%"),'\.cpp') > 0
        "let s:flipname = substitute(expand("%"),'\.cpp\(.*\)','\.hpp\1',"")
        "let s:flipname = substitute(s:flipname, 'Source','Include',"")
    "else
        "let s:flipname = substitute(expand("%"),'\.hpp\(.*\)','\.cpp\1',"")
        "let s:flipname = substitute(s:flipname, 'Include', 'Source', "")
    "endif
    "exe \":e \" s:flipname
"endfun
" ###########################################################################################

function! Switch_HPP_CPP_TestSuite()
    if match(expand("%"),'TestSuite\.cpp') > 0
        let s:flipname = substitute(expand("%"),'TestSuite\.cpp\(.*\)','.cpp\1',"")
        let s:flipname = substitute(s:flipname, 'Test_modules', 'Source', "")
    elseif match(expand("%"),'\.cpp') > 0
        let s:flipname = substitute(expand("%"),'\.cpp\(.*\)','\.hpp\1',"")
        let s:flipname = substitute(s:flipname, 'Source','Include',"")
    elseif match(expand("%"),'\.hpp') > 0
        let s:flipname = substitute(expand("%"),'\.hpp\(.*\)','TestSuite\.cpp\1',"")
        let s:flipname = substitute(s:flipname, 'Include', 'Test_modules', "")
    endif
    exe ":e " s:flipname
endfun
" ###########################################################################################

"Nie trac zaznaczenia przy <  >
noremap    <   <gv
noremap    >   >gv


" tab hint
set wildchar=<TAB> wildmenu wildmode=full
set wildcharm=<C-Z>
nnoremap <TAB> :b <C-Z>

" ###########################################################################################

" ####################################### cscope settings ##################################
set nocscopeverbose
set cscopetag
autocmd VimEnter * :set cscopetagorder=1
" ###########################################################################################

" ####################################### airline Settings ##################################
" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1

" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'

" This allows buffers to be hidden if you've modified a buffer.
" This is almost a must if you wish to use buffers in this way.
set hidden

" To open a new empty buffer
" This replaces :tabnew which I used to bind to this mapping
nmap <leader>T :enew<cr>

" Move to the next buffer
nmap <leader>l <ESC>:bnext<CR>

" Move to the previous buffer
nmap <leader>h <ESC>:bprevious<CR>

" Close the current buffer and move to the previous one
" This replicates the idea of closing a tab
nmap <leader>bq :bp <BAR> bd #<CR>

" close all buffers
nmap <leader>bqq :bufdo bd #<CR>

" Show all open buffers and their status
nmap <leader>bl :ls<CR>
" ###########################################################################################

" ####################################### Ctrl-P Settings ##################################
" Setup some default ignores
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](\.(git|hg|svn)|\_site)$',
  \ 'file': '\v\.(exe|so|dll|class|png|jpg|jpeg)$',
\}

" Use the nearest .git directory as the cwd
" This makes a lot of sense if you are working on a project that is in version
" control. It also supports works with .svn, .hg, .bzr.
let g:ctrlp_working_path_mode = 'r'

" Use a leader instead of the actual named binding
nmap <c-f> :CtrlP C_Application/SC_UEC/<cr>

" Easy bindings for its various modes
"nmap <leader>bb :CtrlPBuffer<cr>
"nmap <leader>bm :CtrlPMixed<cr>
"nmap <leader>bs :CtrlPMRU<cr>

let g:ctrlp_regexp = 1
let g:ctrlp_by_filename = 1

" ###########################################################################################

""""""""""""""""""" KLAWISZOLOGIA: """"""""""""""""""""""""""""""""""
map     <F2> <C-T>
map     <F3> <C-]>
map     <F4> <leader>ci<CR>
"map     <F5> :split<CR>
map     <F6> :vsplit<CR>
map     <F7> :only<CR>
map     <F8> :set wrap!<CR>                 " toggle line wrapping
map     <F9> :set number!<CR>               " przelacza wyswietlanie numerow linii
map     <F10> :call Switch_HPP_CPP_TestSuite()<CR>    " skakanie hpp-cpp
"map    <F11>
map 	<F12> :!cscope -bR<CR>:cs reset<CR><CR>

imap    ii <Esc>

" Make shift-insert work like in Xterm:
map     <S-Insert>      <MiddleMouse>
map!    <S-Insert>      <MiddleMouse>



" Load standard tag files

for tagfile in split(globpath('$PWD/.tags/', '*'), '\n')
        let &tags .= ',' . tagfile
endfor

